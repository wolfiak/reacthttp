import React, {Component} from 'react';
import Post from '../../components/Post/Post';
import './Posts.css'
import {Route} from 'react-router-dom';
import FullPost from '../FullPost/FullPost';
class Posts extends Component{
    state={
        posts: [],
        selectedPostId: null,
        error: false
    }

    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then((res)=>{
                res.json().then((res2)=>{
                    const odp=res2.slice(0,4);
                    const updatePost=odp.map(post =>{
                        return {
                            ...post,
                            author: 'Patriko'
                        }
                    });
                    this.setState({posts: updatePost});
                }).catch(err=> console.log(err));
            }).catch(err=>{
                console.log("Zewnetrzyn");
                console.log(err);
                this.setState({
                    error: true
                })
            });
    }

    clickHandle= (id)=>{
       this.props.history.push({pathname: '/posts/'+id});
    }
    render(){
        let posts= <p style={{textAlign: 'center'}}>Cos poszlo nie tak :(</p>
            if(!this.state.error){
                posts=this.state.posts.map(elm=>{
                    return   <Post key={elm.id} clicked={()=>{ this.clickHandle(elm.id) }} title={elm.title} author={elm.author}/>
                   //  </Link>
                });
            }
        return(
            <div>
                <section className="Posts">
                    {posts}
                </section>
                <Route path={this.props.match.url + '/:id'}  component={FullPost}/>
            </div>
      
        );
    }
}

export default Posts;