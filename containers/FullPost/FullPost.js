import React, { Component } from 'react';

import './FullPost.css';

class FullPost extends Component {

    state={
        loadedPost: null
    }
    componentDidMount(){
        this.loadData();
     
    }
    componentDidUpdate(){
        this.loadData();
    }
    loadData(){
        if(this.props.match.params.id){
            if(!this.state.loadedPost || (this.state.loadedPost && this.state.loadedPost.id !== +this.props.match.params.id)){
                fetch(`https://jsonplaceholder.typicode.com/posts/${this.props.match.params.id}`)
                .then(res=>{
                
                    res.json().then(res2=>{
                            this.setState({
                                loadedPost: res2
                            });
                        });
                })
            }
        }
    }
    deleteDataHandler=()=>{
        fetch(`https://jsonplaceholder.typicode.com/posts/${this.state.loadedPost.id}`,{
            method: 'DELETE',
            mode: 'cors'
        }).then(res=>{
            console.log(res);
        });
    }

    render () {
        let post = <p style={{textAlign: 'center'}}>Please select a Post!</p>;
        if(this.props.match.params.id){
            post = <p style={{textAlign: 'center'}}>Loading...!</p>;
        }
        if(this.state.loadedPost){
            post = (
                <div className="FullPost">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                    <div className="Edit">
                        <button onClick={this.deleteDataHandler} className="Delete">Delete</button>
                    </div>
                </div>

            );
    }
        return post;
    }
}

export default FullPost;